package com.example.datareal;

import org.springframework.web.bind.annotation.*;
import java.util.Date;

@RestController
@RequestMapping(value ="/date")
public class DateController {
    @GetMapping
    public String getCurrentDate() {
        return new Date().toString();
    }
}
